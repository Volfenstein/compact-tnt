package net.moddercoder.compacttnt.network;

import java.util.Set;
import java.util.Collection;

import org.jetbrains.annotations.NotNull;

import net.minecraft.network.PacketByteBuf;

import net.minecraft.util.registry.Registry;

import net.minecraft.particle.ParticleEffect;

import net.moddercoder.compacttnt.reference.Reference;

import net.minecraft.server.network.ServerPlayerEntity;

import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;

public class ParticlePacket {
	public static final void sendParticlePacket (ParticleEffect particleEffect, Collection<ServerPlayerEntity> players, double x, double y, double z, float dx, float dy, float dz) {
		players.forEach((@NotNull var player) -> sendParticlePacket(particleEffect, player, x, y, z, dx, dy, dz));
	}
	public static final void sendParticlePacket (ParticleEffect particleEffect, Set<ServerPlayerEntity> players, double x, double y, double z, float dx, float dy, float dz) {
		players.forEach((@NotNull var player) -> sendParticlePacket(particleEffect, player, x, y, z, dx, dy, dz));
	}
	public static final <T extends ParticleEffect> void sendParticlePacket (T particleEffect, ServerPlayerEntity player, double x, double y, double z, float dx, float dy, float dz) {
		PacketByteBuf buffer = PacketByteBufs.create();
		buffer.writeInt(Registry.PARTICLE_TYPE.getRawId(particleEffect.getType()));
		buffer.writeDouble(x);
		buffer.writeDouble(y);
		buffer.writeDouble(z);
		buffer.writeFloat(dx);
		buffer.writeFloat(dy);
		buffer.writeFloat(dz);
		particleEffect.write(buffer);
		
		ServerPlayNetworking.send(player, Reference.PARTICLE_PACKET, buffer);
	}
}