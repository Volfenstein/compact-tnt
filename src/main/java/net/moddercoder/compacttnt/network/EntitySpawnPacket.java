package net.moddercoder.compacttnt.network;

import net.minecraft.entity.Entity;

import net.minecraft.util.Identifier;

import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.MathHelper;

import net.minecraft.network.Packet;
import net.minecraft.network.PacketByteBuf;

import net.minecraft.util.registry.Registry;

import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;

public class EntitySpawnPacket {
	
	public static Packet<?> create (Entity entity, Identifier packetID) {
		if (entity.world.isClient)
			throw new IllegalStateException("Can't on logical client!");
		
		PacketByteBuf packet = PacketByteBufs.create();
		packet.writeVarInt(Registry.ENTITY_TYPE.getRawId(entity.getType()));
		packet.writeUuid(entity.getUuid());
		packet.writeVarInt(entity.getId());
		
		PacketBufUtil.writeVec3d(packet, entity.getPos());
		PacketBufUtil.writeAngle(packet, entity.getPitch());
		PacketBufUtil.writeAngle(packet, entity.getYaw());
		
		return ServerPlayNetworking.createS2CPacket(packetID, packet);
	}
	
	public static final class PacketBufUtil {
		public static byte packAngle (float angle) {
			return (byte) MathHelper.floor(angle / 360f * 256f);
		}
		public static float unpackAngle (float angleByte) {
			return angleByte / 256f * 360f;
		}
		public static void writeAngle(PacketByteBuf packet, float angle) {
			packet.writeByte(packAngle(angle));
		}
		public static final float readAngle(PacketByteBuf packet) {
			return unpackAngle(packet.readByte());
		}
		public static void writeVec3d(PacketByteBuf packet, Vec3d vec3d) {
			packet.writeDouble(vec3d.x);
			packet.writeDouble(vec3d.y);
			packet.writeDouble(vec3d.z);
		}
		public static Vec3d readVec3d(PacketByteBuf packet) {
			double x = packet.readDouble();
			double y = packet.readDouble();
			double z = packet.readDouble();
			return new Vec3d(x, y, z);
		}
	}
}
