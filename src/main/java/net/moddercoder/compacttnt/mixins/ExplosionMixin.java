package net.moddercoder.compacttnt.mixins;

import java.util.List;

import net.minecraft.world.World;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;

import net.minecraft.entity.mob.Angerable;

import org.jetbrains.annotations.Nullable;

import net.minecraft.util.math.Box;
import net.minecraft.util.math.MathHelper;

import net.minecraft.world.explosion.Explosion;

import net.moddercoder.compacttnt.entity.AbstractCompactTNTEntity;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Explosion.class)
public class ExplosionMixin {
	
	@Final
	@Shadow
	private double x;
	@Final
	@Shadow
	private double y;
	@Final
	@Shadow
	private double z;
	
	@Final
	@Shadow
	private float power;
	
	@Final
	@Shadow
	private World world;
	
	@Final
	@Shadow
	@Nullable
	private Entity entity;
	
	@Inject(method = "collectBlocksAndDamageEntities", at = @At("TAIL"))
	private void collectBlocksAndDamageEntities (CallbackInfo callback) {
		if (entity instanceof AbstractCompactTNTEntity){
			AbstractCompactTNTEntity abstractCompactTNTEntity = (AbstractCompactTNTEntity)entity;
			if (abstractCompactTNTEntity.getCausingEntity() == null)
				return;
			
			float q = this.power * 2f;
			int x1 = MathHelper.floor(this.x - (double)q - 1d);
			int x2 = MathHelper.floor(this.x + (double)q + 1d);
			int y1 = MathHelper.floor(this.y - (double)q - 1d);
			int y2 = MathHelper.floor(this.y + (double)q + 1d);
			int z1 = MathHelper.floor(this.z - (double)q - 1d);
			int z2 = MathHelper.floor(this.z + (double)q + 1d);
			Box box = new Box((double)x1, (double)y1, (double)z1, (double)x2, (double)y2, (double)z2);
			List<Entity> entities = this.world.getOtherEntities(this.entity, box);
			
			entities.forEach(e -> {
				if (e instanceof Angerable)
					((Angerable)e).setAngryAt(abstractCompactTNTEntity.getCausingEntity().getUuid());
			});
		}
		return;
	}
	
	@Inject(method = "getCausingEntity", at = @At("HEAD"), cancellable = true)
	private void getCausingEntity(CallbackInfoReturnable<LivingEntity> callback) {
		if (this.entity instanceof AbstractCompactTNTEntity)
			callback.setReturnValue(((AbstractCompactTNTEntity)this.entity).getCausingEntity());
	}
	
}
