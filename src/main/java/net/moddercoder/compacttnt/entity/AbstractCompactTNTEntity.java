package net.moddercoder.compacttnt.entity;

import java.util.Random;
import java.util.function.Function;

import net.minecraft.network.Packet;

import net.minecraft.world.World;
import net.minecraft.world.GameRules;

import net.minecraft.nbt.NbtCompound;

import net.minecraft.item.ItemStack;
import net.minecraft.item.ShovelItem;

import net.minecraft.util.Hand;
import net.minecraft.util.Identifier;
import net.minecraft.util.ActionResult;

import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.BlockPos;

import net.minecraft.sound.SoundEvents;
import net.minecraft.sound.SoundCategory;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MovementType;

import org.jetbrains.annotations.Nullable;

import net.minecraft.util.registry.Registry;

import net.minecraft.entity.player.PlayerEntity;

import net.minecraft.particle.ParticleTypes;
import net.minecraft.particle.ParticleEffect;
import net.minecraft.particle.DefaultParticleType;

import net.moddercoder.compacttnt.reference.Reference;

import net.fabricmc.fabric.api.networking.v1.PlayerLookup;

import net.moddercoder.compacttnt.network.ParticlePacket;
import net.moddercoder.compacttnt.network.EntitySpawnPacket;

import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;

import net.minecraft.world.explosion.Explosion.DestructionType;

import net.moddercoder.compacttnt.item.CompactTNTThrowableItem;

import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.entity.model.EntityModelLayer;

import net.moddercoder.compacttnt.server.generator.ServerCompactTNTCreature;

//
// This class isn't for extending!
//

public class AbstractCompactTNTEntity extends Entity {
	
	public static final TrackedData<Integer> FUSE;
	
	public void setSettings (ServerCompactTNTCreature.Settings settings, Identifier identifier) {
		this.setExtinguishing(settings.extinguishing());
		if (this.getExtinguishing())
			this.setThrowableItem((CompactTNTThrowableItem)Registry.ITEM.get(identifier));
		this.setImmuneToExplosion(settings.immuneToExplosion());
		this.setParticleListener(settings.particleListener());
		this.setTimerListener(settings.timerListener());
		this.setTickListener(settings.tickListener());
		this.setAttackable(settings.attackable());
		this.setUsePortal(settings.usePortal());
		this.setCollides(settings.collides());
		this.setFuseable(settings.fuseable());
		this.setPushable(settings.pushable());
		this.setFuse(settings.fuseTime());
		this.setSolid(settings.solid());
	}
	
	public void setSettings (AbstractCompactTNTEntity abstractCompactTNTEntity) {
		CompactTNTThrowableItem item = abstractCompactTNTEntity.getThrowableItem();
		this.setSettings(item.tntSettings, item.identifier);
	}
	
	private ParticleListener particleListener = ParticleListener::defaultParticleEvent;
	private TimerListener timerListener = TimerListener::defaultTimerEvent;
	private TickListener tickListener = TickListener::defaultTickEvent;
	@Nullable
	private CompactTNTThrowableItem throwableItem = (CompactTNTThrowableItem)Registry.ITEM.get(new Identifier(Reference.MODID, "dynamite"));
	private Function<Identifier, RenderLayer> renderLayer;
	private boolean immuneToExplosion = true;
	private ParticleManager particleManager;
	private boolean extinguishing = true;
	private int particleStatusValue = 4;
	private EntityModelLayer modelLayer;
	private LivingEntity causingEntity;
	private boolean attackable = true;
	private boolean usePortal = true;
	private boolean collides = true;
	private boolean fuseable = true;
	private boolean pushable = true;
	private boolean solid = false;
	private int maxFuseTimer = 80;
	
	private int fuseTimer;
	
	public AbstractCompactTNTEntity (EntityType<? extends AbstractCompactTNTEntity> entityType, World world) {
		this(entityType, world, 80);
	}
	
	public AbstractCompactTNTEntity (EntityType<? extends AbstractCompactTNTEntity> entityType, World world, int maxFuseTimer) {
		super(entityType, world);
		this.setFuse(maxFuseTimer);
		this.maxFuseTimer = maxFuseTimer;
		this.particleManager = ParticleManager.init(this);
	}
	
	public AbstractCompactTNTEntity(EntityType<? extends AbstractCompactTNTEntity> entityType, World world, double x, double y, double z, LivingEntity igniter, int maxFuseTimer) {
		super(entityType, world);
		
		this.setVelocity(igniter);
		
		this.updatePosition(x, y, z);
		this.setFuse(maxFuseTimer);
		this.prevX = x;
		this.prevY = y;
		this.prevZ = z;
		this.causingEntity = igniter;
		this.maxFuseTimer = maxFuseTimer;
		this.particleManager = ParticleManager.init(this);
	}
	
	@Override
	public void setVelocity (double x, double y, double z) {
		super.setVelocity(x, y, z);
		final float radToDeg = (float) (180.0f / Math.PI);
		double horizontalLen = new Vec3d(x, 0d, z).horizontalLength();
		this.prevYaw = this.getYaw();
		this.prevPitch = this.getPitch();
		this.setYaw((float)Math.atan2(x, z) * radToDeg);
		this.setPitch((float)Math.atan2(y, horizontalLen) * radToDeg);
	}
	
	public void setVelocity (LivingEntity entity) {
		final float degToRad = (float) (Math.PI / 180.0f);
		double dx = -Math.sin(degToRad * entity.getYaw()) * Math.cos(degToRad * entity.getPitch());
		double dz = Math.cos(degToRad * entity.getYaw()) * Math.cos(degToRad * entity.getPitch());
		double dy = -Math.sin(degToRad * entity.getPitch());
	    this.setVelocity(dx, dy, dz);
	    this.setVelocity(this.getVelocity().add(new Vec3d(entity.getVelocity().x, entity.isOnGround() ? entity.getVelocity().y : 0, entity.getVelocity().z)));
	}
	
	@Override
	public ActionResult interact (PlayerEntity player, Hand hand) {
		final ItemStack stack = player.getStackInHand(hand);
		final World world = this.world;
		
		if (this.getExtinguishing()) {
			if (!world.isClient) {
				if (stack.getItem() instanceof ShovelItem) {
					Random random = world.random;
					ItemEntity dropEntity = new ItemEntity(world, getX(), getY() + 0.15d, getZ(), getThrowableItem().getDefaultStack());
					dropEntity.setVelocity((random.nextDouble()*0.2d-random.nextDouble()*0.2d), 0.15d + random.nextDouble()*0.15d, (random.nextDouble()*0.2d-random.nextDouble()*0.2d));
					dropEntity.resetPickupDelay();
					stack.damage(1, player, p -> p.sendToolBreakStatus(hand));
					
					world.playSound(null, getX(), getY(), getZ(), SoundEvents.BLOCK_LAVA_EXTINGUISH, SoundCategory.MASTER, 0.15f, 1f);
					
					this.kill();
					this.world.spawnEntity(dropEntity);
					return ActionResult.SUCCESS;
				}
				return ActionResult.PASS;
			}
		}
		return ActionResult.PASS;
	}
	
	@Override
	protected void initDataTracker() {
		super.dataTracker.startTracking(FUSE, maxFuseTimer);
	}
	
	@Override
	public void tick() {
		super.tick();
		this.tickListener.tick(this, getCausingEntity());
		this.particleListener.handle(this, this.getParticleManager(), random.nextInt(this.getParticleStatusValue()));
	}
	
	@Override
	public void onTrackedDataSet(TrackedData<?> data) {
		if (FUSE.equals(data))
			this.fuseTimer = getFuse();
	}
	
	@Nullable
	public void setCausingEntity(LivingEntity causingEntity) {
		this.causingEntity = causingEntity;
	}
	
	@Nullable
	public LivingEntity getCausingEntity() {
		return this.causingEntity;
	}
	
	public void setFuse(int amount) {
		this.dataTracker.set(FUSE, amount);
		this.fuseTimer = amount;
	}
	
	public boolean isUnderFluid() {
		BlockPos pos = new BlockPos(getPos());
		if (world.getBlockState(pos).getMaterial().isLiquid())
			return true;
		return false;
	}
	
	@Override
	protected void readCustomDataFromNbt(NbtCompound nbt) {
		if (nbt == null) return;
		this.fuseTimer = nbt.getShort("Fuse");
	}
	
	@Override
	protected void writeCustomDataToNbt(NbtCompound nbt) {
		if (nbt == null) return;
		nbt.putShort("Fuse", (short)fuseTimer);
	}
	
	public void explode (float power) {
		if (!this.world.isClient) {
			this.world.createExplosion(this, getX(), getY(), getZ(), power, 
				this.world.getGameRules().getBoolean(GameRules.DO_MOB_GRIEFING) ? DestructionType.BREAK : DestructionType.NONE
			);
		}
	}
	
	public void setRenderLayer(Function<Identifier, RenderLayer> renderLayer) {
		this.renderLayer = renderLayer;
	}
	
	public void setThrowableItem(CompactTNTThrowableItem throwableItem) {
		this.throwableItem = throwableItem;
	}
	
	public void setParticleListener(ParticleListener particleListener) {
		this.particleListener = particleListener;
	}
	
	public void setParticleStatusValue(int particleStatusValue) {
		this.particleStatusValue = particleStatusValue;
	}
	
	public void setImmuneToExplosion(boolean immuneToExplosion) {
		this.immuneToExplosion = immuneToExplosion;
	}
	
	public Function<Identifier, RenderLayer> getRenderLayer() {
		return this.renderLayer;
	}
	
	public void setTimerListener(TimerListener timerListener) {
		this.timerListener = timerListener;
	}
	
	public void setTickListener(TickListener tickListener) {
		this.tickListener = tickListener;
	}
	
	public void setModelLayer(EntityModelLayer modelLayer) {
		this.modelLayer = modelLayer;
	}
	
	public void setExtinguishing(boolean extinguishing) {
		this.extinguishing = extinguishing;
	}
	
	public CompactTNTThrowableItem getThrowableItem() {
		return this.throwableItem;
	}
	
	public ParticleListener getParticleListener() {
		return this.particleListener;
	}
	
	public void setAttackable(boolean attackable) {
		this.attackable = attackable;
	}
	
	public void setUsePortal(boolean usePortal) {
		this.usePortal = usePortal;
	}
	
	public ParticleManager getParticleManager() {
		return this.particleManager;
	}
	
	public void setPushable(boolean pushable) {
		this.pushable = pushable;
	}

	public void setFuseable(boolean fuseable) {
		this.fuseable = fuseable;
	}

	public void setCollides(boolean collides) {
		this.collides = collides;
	}
	
	public TimerListener getTimerListener () {
		return this.timerListener;
	}
	
	public EntityModelLayer getModelLayer() {
		return this.modelLayer;
	}
	
	public TickListener getTickListener() {
		return this.tickListener;
	}
	
	public int getParticleStatusValue() {
		return this.particleStatusValue;
	}
	
	public void setSolid(boolean solid) {
		this.solid = solid;
	}
	
	public boolean getExtinguishing() {
		return this.extinguishing;
	}
	
	public boolean getFuseable() {
		return this.fuseable;
	}
	
	public boolean getSolid() {
		return this.solid;
	}
	
	@Override
	public Packet<?> createSpawnPacket() {
		return EntitySpawnPacket.create(this, Reference.SPAWN_ENTITY_PACKET);
	}
	
	@Override
	public boolean isImmuneToExplosion() {
		return this.immuneToExplosion;
	}
	
	@Override
	public boolean canUsePortals() {
		return this.usePortal;
	}
	
	@Override
	public boolean isAttackable() {
		return this.attackable;
	}
	
	@Override
	public boolean isCollidable() {
		return this.solid;
	}
	
	@Override
	public boolean collides() {
		return !this.isRemoved() && this.collides;
	}
	
	@Override
	public boolean isPushable() {
		return this.pushable;
	}
	
	public int getFuse() {
		return (Integer)dataTracker.get(FUSE);
	}

	static {
		FUSE = DataTracker.registerData(AbstractCompactTNTEntity.class, TrackedDataHandlerRegistry.INTEGER);
	}
	
	public static class ParticleManager {
		
		public static ParticleManager init(AbstractCompactTNTEntity abstractCompactTNTEntity) {
			return new ParticleManager(abstractCompactTNTEntity);
		}
		
		private AbstractCompactTNTEntity abstractCompactTNTEntity;
		
		ParticleManager (AbstractCompactTNTEntity abstractCompactTNTEntity) {
			this.abstractCompactTNTEntity = abstractCompactTNTEntity;
		}
		
		public void sendToClientParticle (ParticleEffect particleEffect, double x, double y, double z, float dx, float dy, float dz) {
			if (!abstractCompactTNTEntity.world.isClient)
				ParticlePacket.sendParticlePacket(particleEffect, PlayerLookup.tracking(abstractCompactTNTEntity), x, y, z, dx, dy, dz);
		}
		
	}
	
	public static interface ParticleListener {
		public static void defaultParticleEvent (AbstractCompactTNTEntity abstractCompactTNTEntity, ParticleManager particleManager, int status) {
			Random random = abstractCompactTNTEntity.getEntityWorld().getRandom();
			if (status == 3) {
				DefaultParticleType particle = abstractCompactTNTEntity.isUnderFluid() ? ParticleTypes.BUBBLE_COLUMN_UP : ParticleTypes.SMOKE;
				particleManager.sendToClientParticle(particle, abstractCompactTNTEntity.getX(), abstractCompactTNTEntity.getY() + 0.55f, abstractCompactTNTEntity.getZ(), (random.nextFloat()-random.nextFloat())*0.05f, 0.05f+random.nextFloat()*0.05f, (random.nextFloat()-random.nextFloat())*0.05f);
			}
		}
		void handle (AbstractCompactTNTEntity abstractCompactTNTEntity, ParticleManager particleManager, int status);
	}
	
	public static interface TimerListener {
		public static void defaultTimerEvent(AbstractCompactTNTEntity abstractCompactTNTEntity, LivingEntity owner) {
			abstractCompactTNTEntity.explode(2.4f);
		}
		void passed(AbstractCompactTNTEntity abstractCompactTNTEntity, LivingEntity owner);
	}
	
	public static interface TickListener {
		public static void defaultTickEvent (AbstractCompactTNTEntity abstractCompactTNTEntity, LivingEntity owner) {
			if (!abstractCompactTNTEntity.hasNoGravity())
				abstractCompactTNTEntity.addVelocity(0.0f, -0.04f, 0.0f);
			
			abstractCompactTNTEntity.move(MovementType.SELF, abstractCompactTNTEntity.getVelocity());
			abstractCompactTNTEntity.setVelocity(abstractCompactTNTEntity.getVelocity().multiply(0.98f));
			
			if (abstractCompactTNTEntity.onGround)
				abstractCompactTNTEntity.setVelocity(abstractCompactTNTEntity.getVelocity().multiply(0.8f, -0.5f, 0.8f));
			
			if (abstractCompactTNTEntity.getFuseable())
				-- abstractCompactTNTEntity.fuseTimer;
			
			if (abstractCompactTNTEntity.fuseTimer <= 0) {
				abstractCompactTNTEntity.timerListener.passed(abstractCompactTNTEntity, owner);
				abstractCompactTNTEntity.kill();
			} else {
				abstractCompactTNTEntity.updateWaterState();
				if (!abstractCompactTNTEntity.world.isClient) {
					abstractCompactTNTEntity.world.sendEntityStatus(abstractCompactTNTEntity, (byte)abstractCompactTNTEntity.random.nextInt(4));
				}
			}
		}
		void tick(AbstractCompactTNTEntity abstractCompactTNTEntity, LivingEntity owner);
	}
	
}