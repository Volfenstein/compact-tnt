package net.moddercoder.compacttnt.item;

import net.minecraft.stat.Stats;

import net.minecraft.world.World;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import net.minecraft.sound.SoundEvents;
import net.minecraft.sound.SoundCategory;

import net.minecraft.block.DispenserBlock;

import net.minecraft.util.Hand;
import net.minecraft.util.Identifier;
import net.minecraft.util.TypedActionResult;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;

import net.moddercoder.compacttnt.entity.AbstractCompactTNTEntity;

import net.moddercoder.compacttnt.server.generator.ServerCompactTNTCreature;

import net.moddercoder.compacttnt.block.dispenser.CompactTNTDipsenserBehavior;

public abstract class CompactTNTThrowableItem extends Item {
	
	public abstract CompactTNTDipsenserBehavior getDispenserBehavior();
	public abstract EntityType<AbstractCompactTNTEntity> getThrowableEntityType();
	
	public Identifier identifier;
	public ServerCompactTNTCreature.Settings tntSettings;
	
	public CompactTNTThrowableItem(Settings settings, final Identifier identifier, ServerCompactTNTCreature.Settings tntSettings) {
		super(settings);
		this.identifier = identifier;
		this.tntSettings = tntSettings;
		DispenserBlock.registerBehavior(this, getDispenserBehavior());
	}
	
	@Override
	public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
		ItemStack stack = user.getStackInHand(hand);
		
		if (!world.isClient) {
			AbstractCompactTNTEntity abstractCompactTNTEntity = getThrowableEntityType().create(world);
			abstractCompactTNTEntity.updatePosition(user.getX(), user.getEyeY(), user.getZ());
			abstractCompactTNTEntity.setCausingEntity(user);
			abstractCompactTNTEntity.setVelocity(user);
			
			abstractCompactTNTEntity.setSettings(tntSettings, identifier);
			
			world.spawnEntity(abstractCompactTNTEntity);
		}
		
		user.incrementStat(Stats.USED.getOrCreateStat(this));
		
		if(!user.isCreative()) {
			stack.decrement(1);
		}
		
		world.playSound(null, user.getX(), user.getY(), user.getZ(), SoundEvents.ITEM_ARMOR_EQUIP_LEATHER, SoundCategory.NEUTRAL, 0.8f, 1f);
		
		return TypedActionResult.success(stack, world.isClient);
	}
	
}