package net.moddercoder.compacttnt.server.generator;

import net.minecraft.item.Item;

import java.util.function.Supplier;

import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import net.moddercoder.compacttnt.CompactTNT;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.entity.EntityDimensions;

import net.moddercoder.compacttnt.item.CompactTNTThrowableItem;

import net.moddercoder.compacttnt.entity.AbstractCompactTNTEntity;

import net.moddercoder.compacttnt.block.dispenser.CompactTNTDipsenserBehavior;

import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;

public class ServerCompactTNTCreature {
	
	private Identifier identifier;
	private ServerCompactTNTCreature.Settings settings;
	
	public ServerCompactTNTCreature(final Identifier identifier, ServerCompactTNTCreature.Settings settings) {
		this.settings = settings;
		this.identifier = identifier;
	}
	
	public CompactTNTThrowableItem registerItem(EntityType<AbstractCompactTNTEntity> entityType) {
		CompactTNTThrowableItem thrownItem = new CompactTNTThrowableItem(settings.itemSettings(), this.getIdentifier(), this.settings()){
			@Override
			public EntityType<AbstractCompactTNTEntity> getThrowableEntityType() {
				return entityType;
			}
			@Override
			public CompactTNTDipsenserBehavior getDispenserBehavior() {
				return settings.dispenserBehavior();
			}
		};
		return Registry.register(Registry.ITEM, getIdentifier(), thrownItem);
	}
	
	public EntityType<AbstractCompactTNTEntity> registerEntityType() {
		EntityType<AbstractCompactTNTEntity> abstractCompactTNTEntityType =
			Registry.register(Registry.ENTITY_TYPE, getIdentifier(), FabricEntityTypeBuilder.<AbstractCompactTNTEntity>create(
				SpawnGroup.MISC, AbstractCompactTNTEntity::new)
					.dimensions(EntityDimensions.fixed(0.3f, 0.55f))
					.trackRangeBlocks(4).trackedUpdateRate(10)
					.build()
			);
		
		settings.dispenserBehavior(() -> CompactTNTDipsenserBehavior.defaultSpawnBehavior(abstractCompactTNTEntityType, settings, identifier));
		
		return abstractCompactTNTEntityType;
	}
	
	public ServerCompactTNTCreature settings(ServerCompactTNTCreature.Settings settings) {
		this.settings = settings;
		return this;
	}
	
	public void setIdentifier(Identifier identifier) {
		this.identifier = identifier;
	}
	
	public ServerCompactTNTCreature.Settings settings() {
		return settings;
	}
	
	public Identifier getIdentifier() {
		return identifier;
	}
	
	public ServerCompactTNTBody build (Supplier<EntityType<AbstractCompactTNTEntity>> entityType, CustomSupplier<CompactTNTThrowableItem, EntityType<AbstractCompactTNTEntity>> throwableItem) {
		return build(entityType.get(), throwableItem);
	}
	
	private ServerCompactTNTBody build (EntityType<AbstractCompactTNTEntity> entityType, CustomSupplier<CompactTNTThrowableItem, EntityType<AbstractCompactTNTEntity>> throwableItem) {
		ServerCompactTNTBody body = new ServerCompactTNTBody (getIdentifier(), settings(), entityType, throwableItem.get(entityType));
		if (ServerManager.BODIES.contains(body))
			return body;
		ServerManager.BODIES.add(body);
		
		CompactTNT.LOGGER.info("["+body.getIdentifier().toString()+"]" + " Successfully builded and added to game!");
		
		return body;
	}
	
	public ServerCompactTNTBody buildAndRegistry () {
		return this.build(this::registerEntityType, this::registerItem);
	}
	
	public static class Settings {
		
		public static Settings of () {
			return new Settings();
		}
		
		private int fuseTime = 80;
		private boolean solid = false;
		private boolean pushable = true;
		private boolean fuseable = true;
		private boolean collides = true;
		private boolean usePortal = true;
		private boolean attackable = true;
		private int particleStatusValue = 4;
		private boolean extinguishing = true;
		private float explosionStrength = 1.6f;
		private boolean immuneToExplosion = true;
		private CompactTNTDipsenserBehavior dispenserBehavior;
		private Item.Settings itemSettings = new Item.Settings();
		private AbstractCompactTNTEntity.TickListener tickListener = AbstractCompactTNTEntity.TickListener::defaultTickEvent;
		private AbstractCompactTNTEntity.TimerListener timerListener = AbstractCompactTNTEntity.TimerListener::defaultTimerEvent;
		private AbstractCompactTNTEntity.ParticleListener particleListener = AbstractCompactTNTEntity.ParticleListener::defaultParticleEvent;
		
		public Settings particleListener(AbstractCompactTNTEntity.ParticleListener particleListener) {
			this.particleListener = particleListener;
			return this;
		}
		
		private Settings dispenserBehavior(Supplier<CompactTNTDipsenserBehavior> dispenserBehavior) {
			this.dispenserBehavior = dispenserBehavior.get();
			return this;
		}
		
		public Settings timerListener(AbstractCompactTNTEntity.TimerListener timerListener) {
			this.timerListener = timerListener;
			return this;
		}
		
		public Settings tickListener(AbstractCompactTNTEntity.TickListener tickListener) {
			this.tickListener = tickListener;
			return this;
		}
		
		public AbstractCompactTNTEntity.ParticleListener particleListener() {
			return this.particleListener;
		}
		
		public AbstractCompactTNTEntity.TimerListener timerListener() {
			return this.timerListener;
		}
		
		public Settings immuneToExplosion(boolean immuneToExplosion) {
			this.immuneToExplosion = immuneToExplosion;
			return this;
		}

		public Settings particleStatusValue(int particleStatusValue) {
			this.particleStatusValue = particleStatusValue;
			return this;
		}
		
		public AbstractCompactTNTEntity.TickListener tickListener() {
			return this.tickListener;
		}

		public Settings explosionStrength(float explosionStrength) {
			this.explosionStrength = explosionStrength;
			return this;
		}
		
		public Settings itemSettings(Item.Settings itemSettings) {
			this.itemSettings = itemSettings;
			return this;
		}
		
		public CompactTNTDipsenserBehavior dispenserBehavior() {
			return this.dispenserBehavior;
		}
		
		public Settings extinguishing(boolean extinguishing) {
			this.extinguishing = extinguishing;
			return this;
		}
		
		public Settings attackable(boolean attackable) {
			this.attackable = attackable;
			return this;
		}
		
		public Settings usePortal(boolean usePortal) {
			this.usePortal = usePortal;
			return this;
		}
		
		public Settings collides(boolean collides) {
			this.collides = collides;
			return this;
		}
		
		public Settings pushable(boolean pushable) {
			this.pushable = pushable;
			return this;
		}
		
		public Settings fuseable(boolean fuseable) {
			this.fuseable = fuseable;
			return this;
		}
		
		public Settings fuseTime(int fuseTime) {
			this.fuseTime = fuseTime;
			return this;
		}
		
		public Settings solid(boolean solid) {
			this.solid = solid;
			return this;
		}
		
		public Item.Settings itemSettings() {
			return this.itemSettings;
		}
		
		public boolean immuneToExplosion() {
			return this.immuneToExplosion;
		}
		
		public int particleStatusValue() {
			return this.particleStatusValue;
		}

		public float explosionStrength() {
			return this.explosionStrength;
		}
		
		public boolean extinguishing() {
			return this.extinguishing;
		}
		
		public boolean attackable() {
			return this.attackable;
		}
		
		public boolean usePortal() {
			return this.usePortal;
		}
		
		public boolean pushable() {
			return this.pushable;
		}

		public boolean fuseable() {
			return this.fuseable;
		}

		public boolean collides() {
			return this.collides;
		}
		
		public boolean solid() {
			return this.solid;
		}
		
		public int fuseTime() {
			return this.fuseTime;
		}
		
	}
	
	@FunctionalInterface
	public static interface CustomSupplier<T, B> {
		T get(B b);
	}
}