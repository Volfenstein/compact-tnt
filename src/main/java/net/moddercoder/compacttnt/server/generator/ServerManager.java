package net.moddercoder.compacttnt.server.generator;

import java.util.ArrayList;

import net.minecraft.util.Identifier;

public class ServerManager {
	
	static final ArrayList<ServerCompactTNTBody> BODIES = new ArrayList<ServerCompactTNTBody>();
	
	public static ServerCompactTNTCreature initCreature (final Identifier identifier, ServerCompactTNTCreature.Settings settings) {
		return new ServerCompactTNTCreature (identifier, settings);
	}
	
	public static ServerCompactTNTCreature emptyCreature (final Identifier identifier) {
		return new ServerCompactTNTCreature(identifier, ServerCompactTNTCreature.Settings.of());
	}
	
	public static ArrayList<ServerCompactTNTBody> getBodies() {
		return BODIES;
	}
}