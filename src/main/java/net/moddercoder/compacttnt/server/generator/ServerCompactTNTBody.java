package net.moddercoder.compacttnt.server.generator;

import net.minecraft.util.Identifier;

import net.minecraft.entity.EntityType;

import net.moddercoder.compacttnt.item.CompactTNTThrowableItem;

import net.moddercoder.compacttnt.entity.AbstractCompactTNTEntity;

public class ServerCompactTNTBody {
	
	private final Identifier identifier;
	private final ServerCompactTNTCreature.Settings settings;
	
	private final CompactTNTThrowableItem throwableItem;
	private final EntityType<AbstractCompactTNTEntity> entityType;
	
	ServerCompactTNTBody (final Identifier identifier, ServerCompactTNTCreature.Settings settings, EntityType<AbstractCompactTNTEntity> entityType, CompactTNTThrowableItem throwableItem) {
		this.settings = settings;
		this.identifier = identifier;
		this.entityType = entityType;
		this.throwableItem = throwableItem;
	}
	
	public final EntityType<AbstractCompactTNTEntity> getEntityType() {
		return entityType;
	}
	
	public final ServerCompactTNTCreature.Settings getSettings () {
		return settings;
	}
	
	public final CompactTNTThrowableItem getThrowableItem() {
		return throwableItem;
	}
	
	public final Identifier getIdentifier() {
		return identifier;
	}
}