package net.moddercoder.compacttnt.server.creatures;

import java.util.Random;

import net.minecraft.world.World;

import net.minecraft.block.Blocks;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;

import net.minecraft.util.Identifier;

import net.minecraft.util.math.Vec3d;

import net.minecraft.sound.SoundEvents;

import net.minecraft.entity.LivingEntity;

import net.minecraft.particle.ParticleTypes;

import net.moddercoder.compacttnt.reference.Reference;

import net.minecraft.particle.ParticleEffect;
import net.minecraft.particle.BlockStateParticleEffect;

import net.moddercoder.compacttnt.entity.AbstractCompactTNTEntity;

import net.moddercoder.compacttnt.util.FireSpread;
import net.moddercoder.compacttnt.util.AbstractCompactTNTEntityUtil;

import net.moddercoder.compacttnt.server.generator.ServerManager;
import net.moddercoder.compacttnt.server.generator.ServerCompactTNTCreature;

public class ServerCompactTNTCreatures {
	
	public static final Identifier MOLOTOV = new Identifier(Reference.MODID, "molotov");
	public static final Identifier DYNAMITE = new Identifier(Reference.MODID, "dynamite");
	public static final Identifier HONEY_BOMB = new Identifier(Reference.MODID, "honey_bomb");
	public static final Identifier PHANTOM_BOMB = new Identifier(Reference.MODID, "phantom_bomb");
	
	public static final Identifier RAW_HONEY_BOMB = new Identifier(Reference.MODID, "raw_honey_bomb");
	public static final Identifier PHANTOM_BOMB_PROJECTILE = new Identifier(Reference.MODID, "phantom_bomb_projectile");
	
	public static final void register () {
		ServerManager.initCreature(DYNAMITE, ServerCompactTNTCreature.Settings.of()
			.itemSettings(new Item.Settings().group(ItemGroup.TOOLS)))
		.buildAndRegistry();
		
		ServerManager.initCreature(MOLOTOV, ServerCompactTNTCreature.Settings.of()
			.itemSettings(new Item.Settings().group(ItemGroup.TOOLS))
			.timerListener(ServerCompactTNTCreatures::molotovExplode)
			.tickListener((entity, owner) -> {
				AbstractCompactTNTEntity.TickListener.defaultTickEvent(entity, owner);
				if (entity.verticalCollision || entity.horizontalCollision)
					molotovExplode(entity, owner);
			})
			.particleListener((entity, manager, status) -> {
				Random random = entity.getEntityWorld().getRandom();
				if (status == 3) {
					ParticleEffect particleEffect = molotovParticleParameter(entity, status);
					manager.sendToClientParticle(particleEffect, entity.getX(), entity.getY() + 0.55f, entity.getZ(), (random.nextFloat()-random.nextFloat())*0.05f, 0.05f+random.nextFloat()*0.05f, (random.nextFloat()-random.nextFloat())*0.05f);
				}
			})
			.particleStatusValue(5)
			.fuseTime(320))
		.buildAndRegistry();
		
		final ServerCompactTNTCreature.Settings RAW_HONEY_BOMB_SETTINGS = ServerCompactTNTCreature.Settings.of()
			.extinguishing(false);
		
		ServerManager.initCreature(RAW_HONEY_BOMB, RAW_HONEY_BOMB_SETTINGS).buildAndRegistry();
		ServerManager.initCreature(HONEY_BOMB, ServerCompactTNTCreature.Settings.of()
			.itemSettings(new Item.Settings().group(ItemGroup.TOOLS))
			.tickListener((entity, owner) -> {
				AbstractCompactTNTEntity.TickListener.defaultTickEvent(entity, owner);
				World world = entity.world;
				
				if (entity.isTouchingWaterOrRain()) {
					AbstractCompactTNTEntity rawHoneyBombEntity = AbstractCompactTNTEntityUtil.ofIdentifier(RAW_HONEY_BOMB, world);
					rawHoneyBombEntity.updatePosition(entity.getX(), entity.getY(), entity.getZ());
					rawHoneyBombEntity.setVelocity(entity.getVelocity());
					rawHoneyBombEntity.setCausingEntity(owner);
					
					rawHoneyBombEntity.setSettings(RAW_HONEY_BOMB_SETTINGS, RAW_HONEY_BOMB);
					
					world.spawnEntity(rawHoneyBombEntity);
					entity.kill();
				}
				
				if (!entity.hasNoGravity()) {
					Vec3d velocity = entity.getVelocity();
					if (entity.horizontalCollision | entity.verticalCollision) {
						entity.setNoGravity(true);
						entity.setVelocity(0d, 0d, 0d);
						for (int i = 0; i < 12; i ++)
							entity.getParticleManager().sendToClientParticle(new BlockStateParticleEffect(ParticleTypes.BLOCK, Blocks.HONEY_BLOCK.getDefaultState()),
								entity.getX(), entity.getY(), entity.getZ(),
								(float)-velocity.x * world.random.nextFloat(), -0.05f * world.random.nextFloat(), (float)-velocity.z * world.random.nextFloat()
							);
						entity.playSound(SoundEvents.BLOCK_HONEY_BLOCK_PLACE, 1f, 1f);
						entity.setPushable(false);
					}
				}
			})
			.particleListener((entity, manager, status) -> {
				AbstractCompactTNTEntity.ParticleListener.defaultParticleEvent(entity, manager, status);
				if (!entity.hasNoGravity()) {
					World world = entity.world;
					Vec3d velocity = entity.getVelocity();
					if (status == 3)
						manager.sendToClientParticle(new BlockStateParticleEffect(ParticleTypes.BLOCK, Blocks.HONEY_BLOCK.getDefaultState()),
							entity.getX(), entity.getY(), entity.getZ(),
							(float)-velocity.x * world.random.nextFloat(), -0.05f * world.random.nextFloat(), (float)-velocity.z * world.random.nextFloat()
						);
				}
			}))
		.buildAndRegistry();
		
		final ServerCompactTNTCreature.Settings PHANTOM_BOMB_PROJECTILE_SETTINGS = ServerCompactTNTCreature.Settings.of()
			.tickListener((entity, owner) -> {
				AbstractCompactTNTEntity.TickListener.defaultTickEvent(entity, owner);
				if (entity.horizontalCollision || entity.verticalCollision) {
					entity.explode(2.4f);
					entity.kill();
				}
			})
			.extinguishing(false);
		
		ServerManager.initCreature(PHANTOM_BOMB_PROJECTILE, PHANTOM_BOMB_PROJECTILE_SETTINGS).buildAndRegistry();
		ServerManager.initCreature(PHANTOM_BOMB, ServerCompactTNTCreature.Settings.of()
			.itemSettings(new Item.Settings().group(ItemGroup.TOOLS))
			.timerListener((entity, owner) -> {
				AbstractCompactTNTEntity.TimerListener.defaultTimerEvent(entity, owner);
				World world = entity.getEntityWorld();
				int count = 3 + world.random.nextInt(2);
				
				for (int i = 0; i < count; i ++) {
					AbstractCompactTNTEntity projectileEntity = AbstractCompactTNTEntityUtil.ofIdentifier(PHANTOM_BOMB_PROJECTILE, world);
					projectileEntity.updatePosition(entity.getX(), entity.getY(), entity.getZ());
					projectileEntity.setCausingEntity(owner);
					
					double dirX = (world.random.nextDouble()-world.random.nextDouble()) * 0.4d;
					double dirY = 0.6d + world.random.nextDouble() * 0.2d;
					double dirZ = (world.random.nextDouble()-world.random.nextDouble()) * 0.4d;
					projectileEntity.setVelocity(dirX, dirY, dirZ);
					
					projectileEntity.setSettings(PHANTOM_BOMB_PROJECTILE_SETTINGS, PHANTOM_BOMB_PROJECTILE);
					
					world.spawnEntity(projectileEntity);
				}
				
				entity.kill();
			})
		).buildAndRegistry();
	}
	
	private static ParticleEffect molotovParticleParameter(AbstractCompactTNTEntity entity, int status) {
		Random random = entity.world.random;
		return entity.isUnderFluid() ? ParticleTypes.BUBBLE_COLUMN_UP : 
			random.nextInt(3) == 1 ? ParticleTypes.FLAME : ParticleTypes.SMOKE;
	}
	
	private static void molotovExplode(AbstractCompactTNTEntity entity, LivingEntity owner) {
		World world = entity.getEntityWorld();
		if (!world.isClient) {
			for (int i = 0, j = 20 + world.getRandom().nextInt(4); i < j; ++ i)
				entity.getParticleManager().sendToClientParticle(ParticleTypes.FLAME, entity.getX(), entity.getY(), entity.getZ(), (world.random.nextFloat()-world.random.nextFloat())*0.11f, 0.2f+world.random.nextFloat()*0.3f, (world.random.nextFloat()-world.random.nextFloat())*0.11f);
			FireSpread.spread(world, entity.getX(), entity.getY(), entity.getZ(), 3f);
			entity.playSound(SoundEvents.BLOCK_GLASS_BREAK, 1f, 1f);
			entity.kill();
		}
	}
}