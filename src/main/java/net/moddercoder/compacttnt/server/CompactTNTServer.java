package net.moddercoder.compacttnt.server;

import net.fabricmc.api.DedicatedServerModInitializer;

import net.moddercoder.compacttnt.server.creatures.ServerCompactTNTCreatures;

public class CompactTNTServer implements DedicatedServerModInitializer {
	
	@Override
	public void onInitializeServer() {
		ServerCompactTNTCreatures.register();
	}
	
}