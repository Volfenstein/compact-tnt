package net.moddercoder.compacttnt.block.dispenser;

import net.minecraft.world.World;

import net.minecraft.item.ItemStack;

import net.minecraft.util.Identifier;

import net.minecraft.entity.EntityType;

import net.minecraft.util.math.Position;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.BlockPointer;

import net.minecraft.block.DispenserBlock;
import net.minecraft.block.dispenser.ItemDispenserBehavior;

import net.moddercoder.compacttnt.entity.AbstractCompactTNTEntity;

import net.moddercoder.compacttnt.util.AbstractCompactTNTEntityUtil;

import net.moddercoder.compacttnt.server.generator.ServerCompactTNTCreature;

public abstract class CompactTNTDipsenserBehavior extends ItemDispenserBehavior {
	
	public static CompactTNTDipsenserBehavior defaultSpawnBehavior (EntityType<AbstractCompactTNTEntity> entityType, ServerCompactTNTCreature.Settings settings, Identifier identifier) {
		return new CompactTNTDipsenserBehavior() {
			@Override
			protected AbstractCompactTNTEntity spawnCompactTNT(World world, Position position, ItemStack stack) {
				return defaultSpawn(entityType, settings, identifier, world, position, stack);
			}
		};
	}
	
	public static AbstractCompactTNTEntity defaultSpawn (EntityType<AbstractCompactTNTEntity> entityType, ServerCompactTNTCreature.Settings settings, Identifier identifier, World world, Position position, ItemStack stack) {
		AbstractCompactTNTEntity abstractCompactTNTEntity = AbstractCompactTNTEntityUtil.ofIdentifier(identifier, world);
		abstractCompactTNTEntity.updatePosition(position.getX(), position.getY(), position.getZ());
		abstractCompactTNTEntity.setSettings(settings, identifier);
		return abstractCompactTNTEntity;
	}
	
	public ItemStack dispenseSilently (BlockPointer pointer, ItemStack stack) {
       World world = pointer.getWorld();
       Position position = DispenserBlock.getOutputLocation(pointer);
       Direction direction = (Direction)pointer.getBlockState().get(DispenserBlock.FACING);
       AbstractCompactTNTEntity abstractCompactTNTEntity = this.spawnCompactTNT(world, position, stack);
       abstractCompactTNTEntity.setVelocity((double)direction.getOffsetX(), (double)((float)direction.getOffsetY() + 0.1f), (double)direction.getOffsetZ());
       world.spawnEntity(abstractCompactTNTEntity);
       stack.decrement(1);
       return stack;
    }
	
	protected abstract AbstractCompactTNTEntity spawnCompactTNT (World world, Position position, ItemStack stack);
	
	protected void playSound(BlockPointer pointer) {
       pointer.getWorld().syncWorldEvent(1002, pointer.getBlockPos(), 0);
    }
	
}