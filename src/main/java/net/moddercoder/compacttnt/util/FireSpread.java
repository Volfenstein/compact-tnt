package net.moddercoder.compacttnt.util;

import java.util.Random;

import net.minecraft.world.World;

import net.minecraft.block.Blocks;

import net.minecraft.block.Material;

import net.minecraft.block.BlockState;

import net.minecraft.util.math.BlockPos;

import net.minecraft.block.AbstractFireBlock;

import net.minecraft.entity.FallingBlockEntity;

public class FireSpread {
	
	public static void spread (World world, double x, double y, double z, float reach) {
		Random rand = world.getRandom();
		
		int i = ((int)reach) * 2;
		
		for (int yy = -2; yy < 3; ++ yy) {
			for (int zz = -(i/2); zz < i/2; ++ zz) {
				for (int xx = -(i/2); xx < i/2; ++ xx) {
					float midDeltaX = (float)Math.abs(x - (x + xx));
					float midDeltaZ = (float)Math.abs(z - (z + zz));
					float midDeltaY = (float)Math.abs(y - (y + yy));
					float midDist = (float)Math.sqrt(midDeltaX*midDeltaX + midDeltaZ*midDeltaZ + midDeltaY*midDeltaY);
					
					BlockPos pos = new BlockPos(x + xx, y - yy, z + zz);
					BlockState state = world.getBlockState(pos);
					BlockState downState = world.getBlockState(pos.down());
					Material material = state.getMaterial();
					Material downMaterial = downState.getMaterial();
					
					if (material.isSolid()) continue;
					
					boolean floor = !downState.isAir() && !downMaterial.isLiquid();
					
					if (floor) {
						if (!world.isClient)
							if (midDist / reach < 0.7f + rand.nextFloat() * 0.3f)
								world.setBlockState(pos, AbstractFireBlock.getState(world, pos));
					} else {
						if (!world.isClient) {
							if (midDist / reach < 0.7f + rand.nextFloat() * 0.3f) {
								FallingBlockEntity fallingFire = new FallingBlockEntity(world, pos.getX(), pos.getY(), pos.getZ(), Blocks.FIRE.getDefaultState());
								fallingFire.timeFalling = 1;
								world.spawnEntity(fallingFire);
							}
						}
					}
				}
			}
		}
	}
}