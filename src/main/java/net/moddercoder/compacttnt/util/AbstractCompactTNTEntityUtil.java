package net.moddercoder.compacttnt.util;

import net.minecraft.world.World;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;

import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import net.moddercoder.compacttnt.entity.AbstractCompactTNTEntity;

public class AbstractCompactTNTEntityUtil {
	
	public static AbstractCompactTNTEntity ofIdentifier (final Identifier identifier, World world) {
		EntityType<?> entityType = Registry.ENTITY_TYPE.get(identifier);
		Entity entity = entityType.create(world);
		
		if (entity instanceof AbstractCompactTNTEntity) {
			AbstractCompactTNTEntity abstractCompactTNTEntity = (AbstractCompactTNTEntity)entity;
			return abstractCompactTNTEntity;
		}
		
		return null;
	}
	
}
