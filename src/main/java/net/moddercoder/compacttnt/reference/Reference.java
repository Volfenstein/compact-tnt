package net.moddercoder.compacttnt.reference;

import net.minecraft.util.Identifier;

public class Reference {
	
	public static final String VERSION = "1.6.1";
	public static final String MODID = "compacttnt";
	public static final String AUTHOR = "moddercoder";
	
	public static final Identifier PARTICLE_PACKET = new Identifier(MODID, "particle_packet");
	public static final Identifier SPAWN_ENTITY_PACKET = new Identifier(MODID, "spawn_packet");
	
}