package net.moddercoder.compacttnt.client.model;

import java.util.function.Function;

import net.minecraft.util.Identifier;

import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;

import net.minecraft.client.util.math.MatrixStack;

import net.minecraft.client.model.Model;
import net.minecraft.client.model.ModelData;
import net.minecraft.client.model.ModelPart;
import net.minecraft.client.model.ModelPartData;
import net.minecraft.client.model.ModelTransform;
import net.minecraft.client.model.ModelPartBuilder;
import net.minecraft.client.model.TexturedModelData;

public class AbstractCompactTNTEntityModel extends Model {
	
	private final ModelPart root;
	
	public AbstractCompactTNTEntityModel(Function<Identifier, RenderLayer> layer, ModelPart root) {
		super(layer);
		this.root = root;
	}
	
	public static TexturedModelData getTexturedModelData(AbstractCompactTNTEntityModel.ModelDataType modelDataType) {
		return modelDataType.getTexturedModelData();
	}
	
	@Override
	public void render(MatrixStack matrices, VertexConsumer vertices, int light, int overlay, float red, float green, float blue, float alpha) {
		//Optimal parameters!
		matrices.translate(-0.03f, 0.19f, -0.03f);
		this.root.render(matrices, vertices, light, overlay, red, green, blue, alpha);
	}
	
	private static final TexturedModelData CUBE_TEXTURED_MODEL_DATA;
	private static final TexturedModelData DEFAULT_TEXTURED_MODEL_DATA;
	private static final TexturedModelData TRANSLUCENT_TEXTURED_MODEL_DATA;
	
	static {
		ModelData dmodelData = new ModelData();
		ModelPartData dmodelPartData = dmodelData.getRoot();
		ModelPartData dmodelPartData2 = dmodelPartData.addChild("body", ModelPartBuilder.create().cuboid(-2, -3, -2, 5, 7, 5), ModelTransform.NONE);
		dmodelPartData2.addChild("wick", ModelPartBuilder.create().cuboid(0, 4, 0, 1, 2, 1), ModelTransform.NONE);
		DEFAULT_TEXTURED_MODEL_DATA = TexturedModelData.of(dmodelData, 32, 32);
		
		ModelData tmodelData = new ModelData();
		ModelPartData tmodelPartData = tmodelData.getRoot();
		ModelPartData tmodelPartData2 = tmodelPartData.addChild("body", ModelPartBuilder.create().cuboid(-2, -3, -2, 5, 7, 5), ModelTransform.NONE);
		tmodelPartData2.addChild("wick", ModelPartBuilder.create().uv(20, 7).cuboid(0, 0, 0, 1, 6, 1), ModelTransform.NONE);
		tmodelPartData2.addChild("fluid", ModelPartBuilder.create().uv(20, 0).cuboid(-1, -2, -1, 3, 4, 3), ModelTransform.NONE);
		TRANSLUCENT_TEXTURED_MODEL_DATA = TexturedModelData.of(tmodelData, 32, 32);
		
		ModelData modelData = new ModelData();
		ModelPartData modelPartData = modelData.getRoot();
		modelPartData.addChild("body", ModelPartBuilder.create().cuboid(0, 0, 0, 4, 4, 4), ModelTransform.NONE);
		CUBE_TEXTURED_MODEL_DATA = TexturedModelData.of(modelData, 32, 32);
	}
	
	public static enum ModelDataType {
		DEFAULT(AbstractCompactTNTEntityModel.DEFAULT_TEXTURED_MODEL_DATA),
		TRANSLUCENT(AbstractCompactTNTEntityModel.TRANSLUCENT_TEXTURED_MODEL_DATA),
		CUBE(AbstractCompactTNTEntityModel.CUBE_TEXTURED_MODEL_DATA);
		
		private TexturedModelData textureModelData;
		
		private ModelDataType (TexturedModelData textureModelData) {
			this.textureModelData = textureModelData;
		}
		
		public TexturedModelData getTexturedModelData() {
			return this.textureModelData;
		}
	}
}