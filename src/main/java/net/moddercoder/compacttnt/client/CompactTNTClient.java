package net.moddercoder.compacttnt.client;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

import net.fabricmc.api.ClientModInitializer;

import net.moddercoder.compacttnt.client.generator.Manager;
import net.moddercoder.compacttnt.client.generator.CompactTNTBody;
import net.moddercoder.compacttnt.client.generator.CompactTNTCreature;

import net.moddercoder.compacttnt.client.creatures.CompactTNTCreatures;

import net.moddercoder.compacttnt.client.network.ParticlePacketClient;
import net.moddercoder.compacttnt.client.network.EntitySpawnPacketClient;

import net.moddercoder.compacttnt.client.model.AbstractCompactTNTEntityModel;

import net.fabricmc.fabric.api.client.rendering.v1.BuiltinItemRendererRegistry;

import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityRendererRegistry;
import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityModelLayerRegistry;

import net.moddercoder.compacttnt.client.renderer.AbstractCompactTNTEntityRenderer;

import net.moddercoder.compacttnt.client.render.entity.model.CompactTNTEntityModelLayers;

@Deprecated
@Environment(EnvType.CLIENT)
public class CompactTNTClient implements ClientModInitializer {
	
	@Override
	public void onInitializeClient () {
		CompactTNTEntityModelLayers.init();
		//Nice, fabric only one class for registering and he is deprecated.
		EntityModelLayerRegistry.registerModelLayer(CompactTNTEntityModelLayers.CUBE_LAYER, () -> AbstractCompactTNTEntityModel.getTexturedModelData(AbstractCompactTNTEntityModel.ModelDataType.CUBE));
		EntityModelLayerRegistry.registerModelLayer(CompactTNTEntityModelLayers.DEFAULT_TNT_LAYER, () -> AbstractCompactTNTEntityModel.getTexturedModelData(AbstractCompactTNTEntityModel.ModelDataType.DEFAULT));
		EntityModelLayerRegistry.registerModelLayer(CompactTNTEntityModelLayers.TRANSLUCENT_TNT_LAYER, () -> AbstractCompactTNTEntityModel.getTexturedModelData(AbstractCompactTNTEntityModel.ModelDataType.TRANSLUCENT));
		
		CompactTNTCreatures.register();
		
		Manager.getBodies().forEach(this::registerBody);
		
		ParticlePacketClient.registerGlobalReceiver();
		EntitySpawnPacketClient.registerGlobalReceiver();
	}
	
	private void registerBody (CompactTNTBody body) {
		CompactTNTCreature.Settings settings = body.getSettings();
		
		BuiltinItemRendererRegistry.INSTANCE.register(body.getThrowableItem(), settings.itemStackRender()::render);
		EntityRendererRegistry.INSTANCE.register(body.getEntityType(), new AbstractCompactTNTEntityRenderer.Builder(settings)::build);
	}
}