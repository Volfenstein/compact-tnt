package net.moddercoder.compacttnt.client.render.entity.model;

import net.minecraft.util.Identifier;

import net.moddercoder.compacttnt.reference.Reference;

import net.minecraft.client.render.entity.model.EntityModelLayer;

public class CompactTNTEntityModelLayers {
	
	public static EntityModelLayer CUBE_LAYER;
	public static EntityModelLayer DEFAULT_TNT_LAYER; 
	public static EntityModelLayer TRANSLUCENT_TNT_LAYER;
	
	public static final void init () {
		CompactTNTEntityModelLayers.CUBE_LAYER = new EntityModelLayer(new Identifier(Reference.MODID, "cube_layer"), "main");
		CompactTNTEntityModelLayers.DEFAULT_TNT_LAYER = new EntityModelLayer(new Identifier(Reference.MODID, "default_tnt_layer"), "main");
		CompactTNTEntityModelLayers.TRANSLUCENT_TNT_LAYER = new EntityModelLayer(new Identifier(Reference.MODID, "translucent_tnt_layer"), "main");
	}
	
}