package net.moddercoder.compacttnt.client.network;

import java.util.UUID;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;

import net.minecraft.util.math.Vec3d;
import net.minecraft.util.registry.Registry;

import net.minecraft.client.world.ClientWorld;

import net.moddercoder.compacttnt.reference.Reference;

import net.moddercoder.compacttnt.network.EntitySpawnPacket;

import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;

public class EntitySpawnPacketClient {
	
	@Environment(EnvType.CLIENT)
	public static void registerGlobalReceiver() {
		ClientPlayNetworking.registerGlobalReceiver(Reference.SPAWN_ENTITY_PACKET, (client, handler, buf, sender) -> {
			EntityType<?> entityType = Registry.ENTITY_TYPE.get(buf.readVarInt());
			UUID uuid = buf.readUuid();
			int entityID = buf.readVarInt();
			Vec3d pos = EntitySpawnPacket.PacketBufUtil.readVec3d(buf);
			float pitch = EntitySpawnPacket.PacketBufUtil.readAngle(buf);
			float yaw = EntitySpawnPacket.PacketBufUtil.readAngle(buf);
			client.execute(() -> {
				ClientWorld clientWorld = client.world;
				if (clientWorld == null)
					throw new IllegalStateException ("Tried to spawn a entity in null world!");
				Entity e = entityType.create(clientWorld);
				if (e == null)
					throw new IllegalStateException ("Failed to create instance of entity \"" + Registry.ENTITY_TYPE.getId(entityType) + "\"!");
				e.updateTrackedPosition(pos);
				e.setPos(pos.x, pos.y, pos.z);
				e.setPitch(pitch);
				e.setYaw(yaw);
				e.setId(entityID);
				e.setUuid(uuid);
				clientWorld.addEntity(entityID, e);
			});
		});
	}
	
}