package net.moddercoder.compacttnt.client.network;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

import net.minecraft.network.PacketByteBuf;

import net.minecraft.util.registry.Registry;

import net.minecraft.particle.ParticleType;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.particle.ParticleEffect;

import net.moddercoder.compacttnt.reference.Reference;

import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;

public class ParticlePacketClient {
	@Environment(EnvType.CLIENT)
	public static void registerGlobalReceiver() {
		ClientPlayNetworking.registerGlobalReceiver(Reference.PARTICLE_PACKET, (client, handler, buf, sender) -> {
			ParticleType<?> particleType = Registry.PARTICLE_TYPE.get(buf.readInt());
			if (particleType == null)
				particleType = ParticleTypes.BARRIER;
			double x = buf.readDouble();
			double y = buf.readDouble();
			double z = buf.readDouble();
			float dx = buf.readFloat();
			float dy = buf.readFloat();
			float dz = buf.readFloat();
			ParticleEffect particleEffect = readParticleParameters(particleType, buf);
			
			client.execute(() -> client.particleManager.addParticle(particleEffect, x, y, z, dx, dy, dz));
		});
	}
	
	@Environment(EnvType.CLIENT)
	private static <T extends ParticleEffect> T readParticleParameters(ParticleType<T> particleType, PacketByteBuf buffer) {
		return particleType.getParametersFactory().read(particleType, buffer);
	}
}