package net.moddercoder.compacttnt.client.generator;

import java.util.ArrayList;

import net.minecraft.util.Identifier;

import net.minecraft.client.render.RenderLayer;

import net.moddercoder.compacttnt.client.render.entity.model.CompactTNTEntityModelLayers;

public class Manager {
	
	static final ArrayList<CompactTNTBody> BODIES = new ArrayList<CompactTNTBody>();
	
	public static CompactTNTCreature initCreature (final Identifier identifier, CompactTNTCreature.Settings settings) {
		return new CompactTNTCreature (identifier, settings);
	}
	
	public static CompactTNTCreature emptyCreatureWithTranslucent (final Identifier identifier) {
		return new CompactTNTCreature(identifier, CompactTNTCreature.Settings.of().renderLayer(RenderLayer::getEntityTranslucent).entityModelLayer(CompactTNTEntityModelLayers.TRANSLUCENT_TNT_LAYER));
	}
	
	public static CompactTNTCreature emptyCreatureWithCube (final Identifier identifier) {
		return new CompactTNTCreature(identifier, CompactTNTCreature.Settings.of().entityModelLayer(CompactTNTEntityModelLayers.CUBE_LAYER));
	}
	
	public static CompactTNTCreature emptyCreature (final Identifier identifier) {
		return new CompactTNTCreature(identifier, CompactTNTCreature.Settings.of());
	}
	
	public static ArrayList<CompactTNTBody> getBodies() {
		return BODIES;
	}
}