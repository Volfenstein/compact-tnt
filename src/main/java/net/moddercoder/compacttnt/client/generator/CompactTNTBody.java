package net.moddercoder.compacttnt.client.generator;

import net.minecraft.util.Identifier;

import net.minecraft.entity.EntityType;

import net.moddercoder.compacttnt.item.CompactTNTThrowableItem;

import net.moddercoder.compacttnt.entity.AbstractCompactTNTEntity;

public class CompactTNTBody {
	
	private final Identifier identifier;
	private final CompactTNTCreature.Settings settings;
	
	private final CompactTNTThrowableItem throwableItem;
	private final EntityType<AbstractCompactTNTEntity> entityType;
	
	CompactTNTBody (final Identifier identifier, CompactTNTCreature.Settings settings, EntityType<AbstractCompactTNTEntity> entityType, CompactTNTThrowableItem throwableItem) {
		this.settings = settings;
		this.identifier = identifier;
		this.entityType = entityType;
		this.throwableItem = throwableItem;
	}
	
	public final EntityType<AbstractCompactTNTEntity> getEntityType() {
		return entityType;
	}
	
	public final CompactTNTCreature.Settings getSettings () {
		return settings;
	}
	
	public final CompactTNTThrowableItem getThrowableItem() {
		return throwableItem;
	}
	
	public final Identifier getIdentifier() {
		return identifier;
	}
}