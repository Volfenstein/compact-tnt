package net.moddercoder.compacttnt.client.generator;

import net.minecraft.item.Item;

import java.util.function.Function;
import java.util.function.Supplier;

import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import net.moddercoder.compacttnt.CompactTNT;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.entity.EntityDimensions;

import net.minecraft.client.render.RenderLayer;

import net.moddercoder.compacttnt.item.CompactTNTThrowableItem;

import net.minecraft.client.render.entity.model.EntityModelLayer;

import net.moddercoder.compacttnt.entity.AbstractCompactTNTEntity;

import net.moddercoder.compacttnt.server.generator.ServerCompactTNTCreature;

import net.moddercoder.compacttnt.block.dispenser.CompactTNTDipsenserBehavior;

import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;

import net.moddercoder.compacttnt.client.render.entity.model.CompactTNTEntityModelLayers;

import net.moddercoder.compacttnt.client.renderer.AbstractCompactTNTEntityRenderer;
import net.moddercoder.compacttnt.client.renderer.AbstractCompactTNTEntityItemStackRenderer;

public class CompactTNTCreature {
	
	private Identifier identifier;
	private CompactTNTCreature.Settings settings;
	
	public static ServerCompactTNTCreature.Settings getServerSettings(CompactTNTCreature.Settings settings) {
		return ServerCompactTNTCreature.Settings.of()
			.fuseTime(settings.fuseTime())
			.solid(settings.solid())
			.pushable(settings.pushable())
			.fuseable(settings.fuseable())
			.collides(settings.collides())
			.usePortal(settings.usePortal())
			.attackable(settings.attackable())
			.itemSettings(settings.itemSettings())
			.tickListener(settings.tickListener())
			.extinguishing(settings.extinguishing())
			.timerListener(settings.timerListener())
			.particleListener(settings.particleListener())
			.explosionStrength(settings.explosionStrength())
			.immuneToExplosion(settings.immuneToExplosion())
			.particleStatusValue(settings.particleStatusValue());
	}
	
	public static ServerCompactTNTCreature.Settings getServerSettings(CompactTNTCreature creature) {
		return CompactTNTCreature.getServerSettings(creature.settings());
	}
	
	CompactTNTCreature(final Identifier identifier, CompactTNTCreature.Settings settings) {
		this.settings = settings;
		this.identifier = identifier;
		final String NAME = identifier.getPath();
		final String ID = identifier.getNamespace();
		
		this.settings.textureIdentifier(new Identifier(ID, "textures/entity/".concat(NAME).concat(".png")));
	}
	
	public CompactTNTThrowableItem registerItem(EntityType<AbstractCompactTNTEntity> entityType) {
		CompactTNTThrowableItem thrownItem = new CompactTNTThrowableItem(settings.itemSettings(), this.getIdentifier(), CompactTNTCreature.getServerSettings(this)){
			@Override
			public EntityType<AbstractCompactTNTEntity> getThrowableEntityType() {
				return entityType;
			}
			@Override
			public CompactTNTDipsenserBehavior getDispenserBehavior() {
				return settings.dispenserBehavior();
			}
		};
		return Registry.register(Registry.ITEM, getIdentifier(), thrownItem);
	}
	
	public EntityType<AbstractCompactTNTEntity> registerEntityType() {
		EntityType<AbstractCompactTNTEntity> abstractCompactTNTEntityType =
			Registry.register(Registry.ENTITY_TYPE, getIdentifier(), FabricEntityTypeBuilder.<AbstractCompactTNTEntity>create(
				SpawnGroup.MISC, AbstractCompactTNTEntity::new)
					.dimensions(EntityDimensions.fixed(0.3f, 0.55f))
					.trackRangeBlocks(4).trackedUpdateRate(10)
					.build()
			);
		
		settings.dispenserBehavior(() -> CompactTNTDipsenserBehavior.defaultSpawnBehavior(abstractCompactTNTEntityType, CompactTNTCreature.getServerSettings(this), identifier));
		
		return abstractCompactTNTEntityType;
	}
	
	public CompactTNTCreature settings(CompactTNTCreature.Settings settings) {
		this.settings = settings;
		return this;
	}
	
	public void setIdentifier(Identifier identifier) {
		this.identifier = identifier;
	}
	
	public CompactTNTCreature.Settings settings() {
		return settings;
	}
	
	public Identifier getIdentifier() {
		return identifier;
	}
	
	public CompactTNTBody build (Supplier<EntityType<AbstractCompactTNTEntity>> entityType, CustomSupplier<CompactTNTThrowableItem, EntityType<AbstractCompactTNTEntity>> throwableItem) {
		return build(entityType.get(), throwableItem);
	}
	
	private CompactTNTBody build (EntityType<AbstractCompactTNTEntity> entityType, CustomSupplier<CompactTNTThrowableItem, EntityType<AbstractCompactTNTEntity>> throwableItem) {
		CompactTNTBody body = new CompactTNTBody (getIdentifier(), settings(), entityType, throwableItem.get(entityType));
		if (Manager.BODIES.contains(body))
			return body;
		Manager.BODIES.add(body);
		
		CompactTNT.LOGGER.info("["+body.getIdentifier().toString()+"]" + " Successfully builded and added to game!");
		
		return body;
	}
	
	public CompactTNTBody buildAndRegistry () {
		return this.build(this::registerEntityType, this::registerItem);
	}
	
	public static class Settings {
		
		public static Settings of () {
			return new Settings();
		}
		
		private int fuseTime = 80;
		private boolean solid = false;
		private boolean pushable = true;
		private boolean fuseable = true;
		private boolean collides = true;
		private boolean usePortal = true;
		private boolean attackable = true;
		private float shadowRadius = 0.05f;
		private int particleStatusValue = 4;
		private float shadowOpacity = 0.25f;
		private boolean extinguishing = true;
		private float explosionStrength = 1.6f;
		private boolean immuneToExplosion = true;
		private CompactTNTDipsenserBehavior dispenserBehavior;
		private Item.Settings itemSettings = new Item.Settings();
		private Function<Identifier, RenderLayer> renderLayer = RenderLayer::getEntitySolid;
		private EntityModelLayer entityModelLayer = CompactTNTEntityModelLayers.DEFAULT_TNT_LAYER;
		private Identifier textureIdentifier = new Identifier ("compacttnt", "textures/entity/compacttnt.png");
		private AbstractCompactTNTEntityRenderer.Renderer entityRenderer = AbstractCompactTNTEntityRenderer::defaultRenderer;		
		private AbstractCompactTNTEntity.TickListener tickListener = AbstractCompactTNTEntity.TickListener::defaultTickEvent;
		private AbstractCompactTNTEntityItemStackRenderer itemStackRender = new AbstractCompactTNTEntityItemStackRenderer(this);
		private AbstractCompactTNTEntity.TimerListener timerListener = AbstractCompactTNTEntity.TimerListener::defaultTimerEvent;
		private AbstractCompactTNTEntity.ParticleListener particleListener = AbstractCompactTNTEntity.ParticleListener::defaultParticleEvent;
		private AbstractCompactTNTEntityItemStackRenderer.ItemStackRenderer itemStackRenderer = AbstractCompactTNTEntityItemStackRenderer::defaultRenderer;
		
		public Settings itemStackRenderer (AbstractCompactTNTEntityItemStackRenderer.ItemStackRenderer itemStackRenderer) {
			this.itemStackRenderer = itemStackRenderer;
			return this;
		}
		
		public Settings particleListener(AbstractCompactTNTEntity.ParticleListener particleListener) {
			this.particleListener = particleListener;
			return this;
		}
		
		private Settings dispenserBehavior(Supplier<CompactTNTDipsenserBehavior> dispenserBehavior) {
			this.dispenserBehavior = dispenserBehavior.get();
			return this;
		}
		
		public Settings itemStackRender(AbstractCompactTNTEntityItemStackRenderer itemStackRender) {
			this.itemStackRender = itemStackRender;
			return this;
		}
		
		public Settings entityRenderer(AbstractCompactTNTEntityRenderer.Renderer entityRenderer) {
			this.entityRenderer = entityRenderer;
			this.itemStackRender = new AbstractCompactTNTEntityItemStackRenderer(this);
			return this;
		}
		
		public AbstractCompactTNTEntityItemStackRenderer.ItemStackRenderer itemStackRenderer () {
			return this.itemStackRenderer;
		}
		
		public Settings timerListener(AbstractCompactTNTEntity.TimerListener timerListener) {
			this.timerListener = timerListener;
			return this;
		}
		
		public Settings tickListener(AbstractCompactTNTEntity.TickListener tickListener) {
			this.tickListener = tickListener;
			return this;
		}
		
		public Settings entityModelLayer(Supplier<EntityModelLayer> entityModelLayer) {
			return this.entityModelLayer(entityModelLayer.get());
		}
		
		public Settings renderLayer (Function<Identifier, RenderLayer> renderLayer) {
			this.renderLayer = renderLayer;
			return this;
		}
		
		public AbstractCompactTNTEntity.ParticleListener particleListener() {
			return this.particleListener;
		}
		
		public AbstractCompactTNTEntityItemStackRenderer itemStackRender() {
			return this.itemStackRender;
		}
		
		public AbstractCompactTNTEntityRenderer.Renderer entityRenderer() {
			return this.entityRenderer;
		}
		
		public Settings entityModelLayer(EntityModelLayer entityModelLayer) {
			this.entityModelLayer = entityModelLayer;
			this.itemStackRender = new AbstractCompactTNTEntityItemStackRenderer(this);
			return this;
		}
		
		public Settings textureIdentifier(Identifier textureIdentifier) {
			this.textureIdentifier = textureIdentifier;
			this.itemStackRender = new AbstractCompactTNTEntityItemStackRenderer(this);
			return this;
		}
		
		public AbstractCompactTNTEntity.TimerListener timerListener() {
			return this.timerListener;
		}
		
		public Settings immuneToExplosion(boolean immuneToExplosion) {
			this.immuneToExplosion = immuneToExplosion;
			return this;
		}

		public Settings particleStatusValue(int particleStatusValue) {
			this.particleStatusValue = particleStatusValue;
			return this;
		}
		
		public AbstractCompactTNTEntity.TickListener tickListener() {
			return this.tickListener;
		}

		public Settings explosionStrength(float explosionStrength) {
			this.explosionStrength = explosionStrength;
			return this;
		}
		
		public Settings itemSettings(Item.Settings itemSettings) {
			this.itemSettings = itemSettings;
			return this;
		}
		
		public Function<Identifier, RenderLayer> renderLayer () {
			return this.renderLayer;
		}
		
		public CompactTNTDipsenserBehavior dispenserBehavior() {
			return this.dispenserBehavior;
		}
		
		public Settings extinguishing(boolean extinguishing) {
			this.extinguishing = extinguishing;
			return this;
		}
		
		public Settings shadowOpacity(int shadowOpacity) {
			this.shadowOpacity = shadowOpacity;
			return this;
		}
		
		public Settings shadowRadius(int shadowRadius) {
			this.shadowRadius = shadowRadius;
			return this;
		}
		
		public Settings attackable(boolean attackable) {
			this.attackable = attackable;
			return this;
		}
		
		public Settings usePortal(boolean usePortal) {
			this.usePortal = usePortal;
			return this;
		}
		
		public Settings collides(boolean collides) {
			this.collides = collides;
			return this;
		}
		
		public Settings pushable(boolean pushable) {
			this.pushable = pushable;
			return this;
		}
		
		public Settings fuseable(boolean fuseable) {
			this.fuseable = fuseable;
			return this;
		}
		
		public EntityModelLayer entityModelLayer() {
			return this.entityModelLayer;
		}
		
		public Settings fuseTime(int fuseTime) {
			this.fuseTime = fuseTime;
			return this;
		}
		
		public Identifier textureIdentifier() {
			return this.textureIdentifier;
		}
		
		public Settings solid(boolean solid) {
			this.solid = solid;
			return this;
		}
		
		public Item.Settings itemSettings() {
			return this.itemSettings;
		}
		
		public boolean immuneToExplosion() {
			return this.immuneToExplosion;
		}
		
		public int particleStatusValue() {
			return this.particleStatusValue;
		}

		public float explosionStrength() {
			return this.explosionStrength;
		}
		
		public boolean extinguishing() {
			return this.extinguishing;
		}
		
		public float shadowOpacity() {
			return this.shadowOpacity;
		}
		
		public float shadowRadius() {
			return this.shadowRadius;
		}
		
		public boolean attackable() {
			return this.attackable;
		}
		
		public boolean usePortal() {
			return this.usePortal;
		}
		
		public boolean pushable() {
			return this.pushable;
		}

		public boolean fuseable() {
			return this.fuseable;
		}

		public boolean collides() {
			return this.collides;
		}
		
		public boolean solid() {
			return this.solid;
		}
		
		public int fuseTime() {
			return this.fuseTime;
		}
		
	}
	
	@FunctionalInterface
	public static interface CustomSupplier<T, B> {
		T get(B b);
	}
}