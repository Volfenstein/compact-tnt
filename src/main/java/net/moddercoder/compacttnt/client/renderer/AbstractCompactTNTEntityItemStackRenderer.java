package net.moddercoder.compacttnt.client.renderer;

import java.util.function.Function;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

import net.minecraft.item.ItemStack;

import net.minecraft.util.Identifier;

import net.minecraft.client.MinecraftClient;

import net.minecraft.client.util.math.MatrixStack;

import net.minecraft.client.render.item.ItemRenderer;

import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;

import net.minecraft.client.render.entity.model.EntityModelLayer;
import net.minecraft.client.render.entity.model.EntityModelLoader;

import net.moddercoder.compacttnt.client.generator.CompactTNTCreature;

import net.minecraft.client.render.model.json.ModelTransformation.Mode;

import net.moddercoder.compacttnt.client.model.AbstractCompactTNTEntityModel;

@Environment(EnvType.CLIENT)
public class AbstractCompactTNTEntityItemStackRenderer {
	
	private final ItemStackRenderer RENDERER;
	private final CompactTNTCreature.Settings SETTINGS;
	
	private final Identifier TEXTURE_IDENTIFIER;
	private final EntityModelLayer ENTITY_MODEL_LAYER;
	private final Function<Identifier, RenderLayer> RENDER_LAYER;
	
	public AbstractCompactTNTEntityItemStackRenderer(final CompactTNTCreature.Settings SETTINGS) {
		this.SETTINGS = SETTINGS;
		this.RENDER_LAYER = SETTINGS.renderLayer();
		this.RENDERER = this.SETTINGS.itemStackRenderer();
		this.ENTITY_MODEL_LAYER = SETTINGS.entityModelLayer();
		this.TEXTURE_IDENTIFIER = SETTINGS.textureIdentifier();
	}
	
	public void render(ItemStack stack, Mode mode, MatrixStack matrices, VertexConsumerProvider vertexConsumersProvider, int light, int overlay) {
		this.RENDERER.render(this.RENDER_LAYER, this.ENTITY_MODEL_LAYER, this.TEXTURE_IDENTIFIER, mode, matrices, vertexConsumersProvider, light, overlay);
	}
	
	public static void defaultRenderer (Function<Identifier, RenderLayer> renderLayer, EntityModelLayer entityModelLayer, Identifier textureIdentifier, Mode mode, MatrixStack matrices, VertexConsumerProvider vertexConsumersProvider, int light, int overlay) {
		matrices.push();
		final EntityModelLoader LOADER = MinecraftClient.getInstance().getEntityModelLoader();
		final AbstractCompactTNTEntityModel MODEL = new AbstractCompactTNTEntityModel(renderLayer, LOADER.getModelPart(entityModelLayer));
		VertexConsumer vConsumer = ItemRenderer.getDirectItemGlintConsumer(vertexConsumersProvider, MODEL.getLayer(textureIdentifier), false, false);
		MODEL.render(matrices, vConsumer, light, overlay, 1f, 1f, 1f, 1f);
		matrices.pop();
	}
	
	public static interface ItemStackRenderer {
		void render (Function<Identifier, RenderLayer> renderLayer, EntityModelLayer entityModelLayer, Identifier textureIdentifier, Mode mode, MatrixStack matrices, VertexConsumerProvider vertexConsumersProvider, int light, int overlay);
	}
}
