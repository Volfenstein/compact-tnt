package net.moddercoder.compacttnt.client.renderer;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

import net.minecraft.util.Identifier;
import net.minecraft.util.math.Vec3f;
import net.minecraft.client.model.Model;

import net.minecraft.client.util.math.MatrixStack;

import net.minecraft.client.render.OverlayTexture;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.item.ItemRenderer;
import net.minecraft.client.render.VertexConsumerProvider;

import net.minecraft.client.render.entity.EntityRenderer;
import net.minecraft.client.render.entity.EntityRendererFactory;

import net.moddercoder.compacttnt.entity.AbstractCompactTNTEntity;

import net.minecraft.client.render.entity.model.EntityModelLoader;

import net.moddercoder.compacttnt.client.generator.CompactTNTCreature;

import net.moddercoder.compacttnt.client.model.AbstractCompactTNTEntityModel;

@Environment(EnvType.CLIENT)
public class AbstractCompactTNTEntityRenderer extends EntityRenderer<AbstractCompactTNTEntity> {
	
	private final EntityModelLoader LOADER;
	private final CompactTNTCreature.Settings SETTINGS;
	
	private AbstractCompactTNTEntityRenderer(EntityRendererFactory.Context context, final CompactTNTCreature.Settings SETTINGS) {
		super(context);
		this.SETTINGS = SETTINGS;
		this.LOADER = context.getModelLoader();
	}
	
	@Override
	public void render(AbstractCompactTNTEntity entity, float yaw, float tickDelta, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light) {
		AbstractCompactTNTEntityModel abstractCompactTNTEntityModel = new AbstractCompactTNTEntityModel(this.SETTINGS.renderLayer(), LOADER.getModelPart(this.SETTINGS.entityModelLayer()));
		this.SETTINGS.entityRenderer().render(entity, abstractCompactTNTEntityModel, this.SETTINGS.textureIdentifier(), yaw, tickDelta, matrices, vertexConsumers, light);
		super.render(entity, yaw, tickDelta, matrices, vertexConsumers, light);
	}
	
	@Override
	public Identifier getTexture(AbstractCompactTNTEntity entity) {
		return this.SETTINGS.textureIdentifier();
	}
	
	public static void defaultRenderer (AbstractCompactTNTEntity entity, final Model MODEL, final Identifier TEXTURE_IDENTIFIER, float yaw, float tickDelta, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light) {
		matrices.push();
			matrices.multiply(Vec3f.POSITIVE_Y.getDegreesQuaternion(entity.getYaw() - 225));
			
			VertexConsumer vertexConsumer = ItemRenderer.getDirectItemGlintConsumer(vertexConsumers, MODEL.getLayer(TEXTURE_IDENTIFIER), false, false);
			
			MODEL.render(matrices, vertexConsumer, light, OverlayTexture.DEFAULT_UV, 1.0f, 1.0f, 1.0f, 1.0f);
		matrices.pop();
	}
	
	public static interface Renderer {
		void render(AbstractCompactTNTEntity entity, final Model MODEL, final Identifier TEXTURE_IDENTIFIER, float yaw, float tickDelta, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light);
	}
	
	public static class Builder {
		
		private CompactTNTCreature.Settings settings;
		
		public Builder (CompactTNTCreature.Settings settings) {
			this.settings = settings;
		}
		
		public AbstractCompactTNTEntityRenderer build(EntityRendererFactory.Context context) {
			AbstractCompactTNTEntityRenderer abstractCompactTNTEntityRenderer = new AbstractCompactTNTEntityRenderer(context, settings);
			return abstractCompactTNTEntityRenderer;
		}
		
		public CompactTNTCreature.Settings getSettings() {
			return this.settings;
		}
	}
}